#!/bin/bash
TITLE="Tethering over USB script"
VERSION="v.0.3"

#tethering over usb
echo "" >> /etc/network/interfaces ;
echo "allow-hotplug eth1" >> /etc/network/interfaces ;
echo "iface eth1 inet dhcp" >> /etc/network/interfaces ;

apt-get update ;
apt-get upgrade -y ;
apt-get install ipheth-utils -y ;

echo "" ;
echo "RPi ip address = 172.20.10.2" ;

#===============================================================
#				add info to MOTD
#===============================================================
if [ ! -f /etc/motd_static ] ; then 
  rm /etc/motd ;
  ln -s /etc/motd_static /etc/motd ;
  touch /etc/motd_static ;
  echo "" >> /etc/motd_static ;
else
  sed -i '$ d' /etc/motd_static ; #remove last empty line
fi
NOW=$(date +"%m-%d-%Y")
cat >> /etc/motd_static <<END_MOTD
$TITLE $VERSION @$NOW
END_MOTD
echo "" >> /etc/motd_static ;

# post notification to slack
source utils/slackpost_config.sh ;
/bin/bash utils/slackpost.sh $WEBHOOK_URL $CHANNEL $SLACK_NAME "Finished: $TITLE" ;
