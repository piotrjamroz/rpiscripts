#!/bin/bash
TITLE="Essentials script"
VERSION="v.0.4"

apt-get update ;
apt-get upgrade -y ;

apt-get install nano screen htop curl -y ;


#===============================================================
#				add info to MOTD
#===============================================================
if [ ! -f /etc/motd_static ] ; then 
  rm /etc/motd ;
  ln -s /etc/motd_static /etc/motd ;
  touch /etc/motd_static ;
  echo "" >> /etc/motd_static ;
else
  sed -i '$ d' /etc/motd_static ; #remove last empty line
fi
NOW=$(date +"%m-%d-%Y")
cat >> /etc/motd_static <<END_MOTD
$TITLE $VERSION @$NOW
END_MOTD
echo "" >> /etc/motd_static ;

# post notification to slack
source utils/slackpost_config.sh ;
/bin/bash utils/slackpost.sh $WEBHOOK_URL $CHANNEL $SLACK_NAME "Finished: $TITLE" ;