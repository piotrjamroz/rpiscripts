#!/bin/bash
TITLE="LibreNMS script"
VERSION="v.0.1"
WORKING_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# TODO: add global variables
################
#
################

apt-get update
apt-get upgrade -y

apt-get install raspi-config mariadb-server mariadb-client -y

# TODO: check if needed
# run raspi-config and set time zone!

service mysql restart

mysql -u root -p <<EOF
CREATE DATABASE librenms;
GRANT ALL PRIVILEGES ON librenms.* TO 'root'@'localhost' IDENTIFIED BY 'stokrotka.123';
FLUSH PRIVILEGES;
exit
EOF

# nano /etc/mysql/my.cnf

# Add the follow lines to the mariadb.cnf file under the [mysqld] section
#========================================================================
# innodb_file_per_table=1
# sql_mode=''

# TODO: check if ok then replace above
# cat >> /etc/mysql/my.cnf <<END_CONF
# [mysqld]
# innodb_file_per_table=1
# sql_mode=''
# END_CONF

service mysql restart

apt-get install libapache2-mod-php php-cli php-mysql php-gd php-snmp php-pear php-curl snmp graphviz php-mcrypt php-json apache2 fping imagemagick whois mtr-tiny nmap python-mysqldb snmpd php-net-ipv4 php-net-ipv6 rrdtool git -y

# !!! before populating librenms database
# set timezone in /etc/php/7.0/cli/php.ini
# date.timezone ='Europe/Warsaw'

mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.old

# nano /etc/snmp/snmpd.conf

# configure SNMP
#=============================
# rocommunity machina
# syslocation Legionow
# syscontact Machinarius

service snmpd restart

useradd librenms -d /opt/librenms -M -r
usermod -a -G librenms www-data

cd /opt

git clone https://github.com/librenms/librenms.git librenms

cd /opt/librenms

# already has this directory, might not be needed
chown -R librenms:librenms /opt/librenms
#chmod 775 rrd

chmod ug+rw /opt/librenms/rrd
chmod ug+rw /opt/librenms/logs

nano /etc/apache2/sites-available/librenms.conf

#<VirtualHost *:80>
# DocumentRoot /opt/librenms/html/
# ServerName librenms.example.com
# CustomLog /opt/librenms/logs/access_log combined
# ErrorLog /opt/librenms/logs/error_log
# AllowEncodedSlashes NoDecode
# <Directory "/opt/librenms/html/">
# Require all granted
# AllowOverride All
# Options FollowSymLinks MultiViews
# </Directory>
# </VirtualHost>

phpenmod mcrypt

a2ensite librenms.conf
a2enmod rewrite
service apache2 restart

a2dissite 000-default
service apache2 reload

cp config.php.default config.php

nano config.php

# Change the values to the right of the equal sign for lines beginning with $config[db_] to match your database information as setup above.

# Change the value of $config['snmp']['community'] from public to whatever your read-only SNMP community is.

php build-base.php

php adduser.php piotrjamroz stokrotka.123 10 piotrjamroz@wp.pl
php addhost.php machina public v2c


#fix for fping & frping6 v 3.x
chown root:root /usr/bin/fping
chmod 4755 /usr/bin/fping

chown root:root /usr/bin/fping6
chmod 4755 /usr/bin/fping6

# check if instalation ok
php validate.php

php discovery.php -h all

# TODO: add Cron job

cp librenms.nonroot.cron /etc/cron.d/librenms


#===============================================================
#				add info to MOTD
#===============================================================
# if [ ! -f /etc/motd_static ] ; then 
#   rm /etc/motd 
#   ln -s /etc/motd_static /etc/motd 
#   touch /etc/motd_static
#   echo "" >> /etc/motd_static
# else
#   sed -i '$ d' /etc/motd_static #remove last empty line
# fi
# NOW=$(date +"%m-%d-%Y")
# cat >> /etc/motd_static <<END_MOTD
# $TITLE $VERSION @$NOW
# END_MOTD
# echo "" >> /etc/motd_static

# # post notification to slack
# source $WORKING_DIR/utils/slackwebhook.sh
# /bin/bash $WORKING_DIR/utils/slackpost.sh $WEBHOOK_URL $CHANNEL $SLACK_NAME "Finished: $TITLE"