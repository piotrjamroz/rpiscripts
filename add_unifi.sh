#!/bin/bash
TITLE="UniFi script"
VERSION="v.0.4"
WORKING_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo 'deb http://www.ubnt.com/downloads/unifi/debian stable ubiquiti' | tee -a /etc/apt/sources.list.d/ubnt.list > /dev/null
apt-key adv --keyserver keyserver.ubuntu.com --recv C0A52C50
apt-get update

apt-get install unifi -y

# disable the default MongoDB database instance
echo 'ENABLE_MONGODB=no' | tee -a /etc/mongodb.conf > /dev/null

# uncomment if RPi Zero
# rm /usr/lib/unifi/lib/native/Linux/armhf/libubnt_webrtc_jni.so

# update to Oracle Java 8
apt-get install oracle-java8-jdk -y
echo 'JAVA_HOME=/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt' | tee /etc/default/unifi > /dev/null

echo "customize Unifi: /var/lib/unifi/system.properties"
echo "access UniFi at https://<RPi_IP>:8443"

#===============================================================
#				add info to MOTD
#===============================================================
if [ ! -f /etc/motd_static ] ; then 
  rm /etc/motd 
  ln -s /etc/motd_static /etc/motd 
  touch /etc/motd_static
  echo "" >> /etc/motd_static
else
  sed -i '$ d' /etc/motd_static #remove last empty line
fi
NOW=$(date +"%m-%d-%Y")
cat >> /etc/motd_static <<END_MOTD
$TITLE $VERSION @$NOW
END_MOTD
echo "" >> /etc/motd_static

# post notification to slack
source utils/slackpost_config.sh
/bin/bash $WORKING_DIR/utils/slackpost.sh $WEBHOOK_URL $CHANNEL $SLACK_NAME "Finished: $TITLE"