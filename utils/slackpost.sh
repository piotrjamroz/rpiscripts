#!/bin/bash

# Usage: slackpost "<webhook_url>" "<channel>" "<username>" "<message>"

# ------------
VERSION="v.0.1"

webhook_url=$1
if [[ $webhook_url == "" ]]
then
        echo "No webhook_url specified"
        echo "Usage: slackpost "<webhook_url>" "<channel>" "<username>" "<message>""
        exit 1
fi

# ------------
shift
channel=$1
if [[ $channel == "" ]]
then
        echo "No channel specified"
        echo "Usage: slackpost "<webhook_url>" "<channel>" "<username>" "<message>""
        exit 1
fi

# ------------
shift
username=$1
if [[ $username == "" ]]
then
        echo "No username specified"
        echo "Usage: slackpost "<webhook_url>" "<channel>" "<username>" "<message>""
        exit 1
fi

# ------------
shift

text=$*

if [[ $text == "" ]]
then
        echo "No text specified"
        echo "Usage: slackpost "<webhook_url>" "<channel>" "<username>" "<message>""
        exit 1
fi

escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" )

json="{\"channel\": \"$channel\", \"username\":\"$username\", \"icon_emoji\": \":space_invader:\", \"text\": \"$escapedText\"}"

curl -s -d "payload=$json" "$webhook_url"