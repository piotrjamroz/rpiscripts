#!/bin/bash

# Usage: discordpost "<webhook_url>" "<message>"

# ------------
VERSION="v.0.1"

webhook_url=$1
if [[ $webhook_url == "" ]]
then
        echo "No webhook_url specified"
        echo "Usage: slackpost "<webhook_url>" "<message>""
        exit 1
fi

# ------------
shift

message=$*

if [[ $message == "" ]]
then
        echo "No text specified"
        echo "Usage: slackpost "<webhook_url>" "<message>""
        exit 1
fi


message="$1"
## format to parse to curl
## echo Sending message: $message
msg_content=\"$message\"

## discord webhook
curl -H "Content-Type: application/json" -X POST -d "{\"content\": $msg_content}" $webhook_url