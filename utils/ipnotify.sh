#!/bin/bash
VERSION="v.0.1"

WEBHOOK_URL="https://hooks.slack.com/services/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"
CHANNEL="rpi"
SLACK_NAME="RPi"

# ------------
INTERFACE="eth0"

IP=$(/sbin/ip -o -4 addr list $INTERFACE | awk '{print $4}' | cut -d/ -f1)

# ------------
text="Up & running @$IP"
escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" )
json="{\"channel\": \"$channel\", \"username\":\"$username\", \"icon_emoji\": \":space_invader:\", \"text\": \"$escapedText\"}"

# post ip address to slack
curl -s -d "payload=$json" "$WEBHOOK_URL"





